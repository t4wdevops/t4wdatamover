USE [T4WStage]
GO

/****** Object:  Table [dbo].[T4W_parent]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_parent]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_parent]
GO

/****** Object:  Table [dbo].[T4W_orders_items_refunds]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_orders_items_refunds]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_orders_items_refunds]
GO

/****** Object:  Table [dbo].[T4W_orders_items]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_orders_items]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_orders_items]
GO

/****** Object:  Table [dbo].[T4W_orders]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_orders]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_orders]
GO

/****** Object:  Table [dbo].[T4W_mdl_user_info_data]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_user_info_data]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_user_info_data]
GO

/****** Object:  Table [dbo].[T4W_mdl_user_enrolments]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_user_enrolments]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_user_enrolments]
GO

/****** Object:  Table [dbo].[T4W_mdl_user]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_user]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_user]
GO

/****** Object:  Table [dbo].[T4W_mdl_role_assignments]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_role_assignments]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_role_assignments]
GO

/****** Object:  Table [dbo].[T4W_mdl_role]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_role]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_role]
GO

/****** Object:  Table [dbo].[T4W_mdl_enrol]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_enrol]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_enrol]
GO

/****** Object:  Table [dbo].[T4W_mdl_course]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_course]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_course]
GO

/****** Object:  Table [dbo].[T4W_mdl_context]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_context]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_context]
GO

/****** Object:  Table [dbo].[T4W_mdl_certificate_issues]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_certificate_issues]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_certificate_issues]
GO

/****** Object:  Table [dbo].[T4W_mdl_certificate]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_mdl_certificate]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_mdl_certificate]
GO

/****** Object:  Table [dbo].[T4W_dormancy]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_dormancy]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_dormancy]
GO

/****** Object:  Table [dbo].[T4W_context_history]    Script Date: 11/20/2020 10:38:49 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[T4W_context_history]') AND type in (N'U'))
DROP TABLE [dbo].[T4W_context_history]
GO

/****** Object:  Table [dbo].[T4W_context_history]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_context_history](
	[id] [int] NOT NULL,
	[contextid] [int] NULL,
	[courseid] [int] NULL,
	[userid] [int] NULL,
	[timestamp] [datetime2](7) NULL,
	[endstamp] [datetime2](7) NULL,
	[is_teacher] [tinyint] NULL,
 CONSTRAINT [PK_T4W_context_history] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_dormancy]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_dormancy](
	[id] [int] NOT NULL,
	[created] [datetime2](7) NULL,
	[mraid] [int] NULL,
	[dormancy_start] [datetime2](7) NULL,
	[dormancy_end] [datetime2](7) NULL,
	[active] [int] NULL,
 CONSTRAINT [PK_T4W_dormancy] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_certificate]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_certificate](
	[id] [bigint] NOT NULL,
	[course] [bigint] NULL,
	[name] [nvarchar](255) NULL,
	[intro] [ntext] NULL,
	[introformat] [smallint] NULL,
	[emailteachers] [tinyint] NULL,
	[emailothers] [ntext] NULL,
	[savecert] [tinyint] NULL,
	[reportcert] [tinyint] NULL,
	[reissuecert] [tinyint] NULL,
	[delivery] [smallint] NULL,
	[certificatetype] [nvarchar](50) NOT NULL,
	[orientation] [nvarchar](10) NULL,
	[borderstyle] [nvarchar](30) NULL,
	[bordercolor] [nvarchar](30) NULL,
	[printwmark] [nvarchar](30) NULL,
	[printdate] [bigint] NULL,
	[datefmt] [bigint] NULL,
	[printnumber] [bigint] NULL,
	[printgrade] [bigint] NULL,
	[gradefmt] [bigint] NULL,
	[printoutcome] [bigint] NULL,
	[printhours] [ntext] NULL,
	[printteacher] [bigint] NULL,
	[customtext] [ntext] NULL,
	[printsignature] [nvarchar](30) NULL,
	[printseal] [nvarchar](30) NULL,
	[timemodified] [datetime2](7) NULL,
 CONSTRAINT [PK_T4W_mdl_certificate] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_certificate_issues]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_certificate_issues](
	[id] [bigint] NOT NULL,
	[certificateid] [bigint] NULL,
	[userid] [bigint] NULL,
	[timecreated] [datetime2](7) NULL,
	[studentname] [nvarchar](40) NOT NULL,
	[code] [nvarchar](40) NULL,
	[classname] [nvarchar](254) NOT NULL,
	[certdate] [datetime2](7) NULL,
	[reportgrade] [nvarchar](10) NULL,
	[mailed] [tinyint] NULL,
 CONSTRAINT [PK_T4W_mdl_certificate_issues] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_context]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_context](
	[id] [bigint] NOT NULL,
	[contextlevel] [bigint] NULL,
	[instanceid] [bigint] NULL,
	[path] [nvarchar](255) NULL,
	[depth] [tinyint] NULL,
 CONSTRAINT [PK_T4W_mdl_context] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_course]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_course](
	[id] [bigint] NOT NULL,
	[category] [bigint] NULL,
	[sortorder] [bigint] NULL,
	[fullname] [nvarchar](254) NULL,
	[shortname] [nvarchar](255) NULL,
	[idnumber] [nvarchar](100) NULL,
	[summary] [ntext] NULL,
	[summaryformat] [tinyint] NULL,
	[format] [nvarchar](21) NULL,
	[showgrades] [tinyint] NULL,
	[newsitems] [int] NULL,
	[startdate] [datetime2](7) NULL,
	[marker] [bigint] NULL,
	[maxbytes] [bigint] NULL,
	[legacyfiles] [smallint] NULL,
	[showreports] [smallint] NULL,
	[visible] [tinyint] NULL,
	[visibleold] [tinyint] NULL,
	[groupmode] [smallint] NULL,
	[groupmodeforce] [smallint] NULL,
	[defaultgroupingid] [bigint] NULL,
	[lang] [nvarchar](30) NULL,
	[theme] [nvarchar](50) NULL,
	[timecreated] [datetime2](7) NULL,
	[timemodified] [datetime2](7) NULL,
	[requested] [tinyint] NULL,
	[enablecompletion] [tinyint] NULL,
	[completionnotify] [tinyint] NULL,
	[cacherev] [bigint] NULL,
	[activityflow] [tinyint] NULL,
	[calendartype] [nvarchar](30) NULL,
 CONSTRAINT [PK_T4W_mdl_course] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_enrol]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_enrol](
	[id] [bigint] NOT NULL,
	[enrol] [nvarchar](20) NULL,
	[status] [bigint] NULL,
	[courseid] [bigint] NULL,
	[sortorder] [bigint] NULL,
	[name] [nvarchar](255) NULL,
	[enrolperiod] [bigint] NULL,
	[enrolstartdate] [datetime2](7) NULL,
	[enrolenddate] [datetime2](7) NULL,
	[expirynotify] [tinyint] NULL,
	[expirythreshold] [bigint] NULL,
	[notifyall] [tinyint] NULL,
	[password] [nvarchar](50) NULL,
	[cost] [nvarchar](20) NULL,
	[currency] [nvarchar](3) NULL,
	[roleid] [bigint] NULL,
	[customint1] [bigint] NULL,
	[customint2] [bigint] NULL,
	[customint3] [bigint] NULL,
	[customint4] [bigint] NULL,
	[customint5] [bigint] NULL,
	[customint6] [bigint] NULL,
	[customint7] [bigint] NULL,
	[customint8] [bigint] NULL,
	[customchar1] [nvarchar](255) NULL,
	[customchar2] [nvarchar](255) NULL,
	[customchar3] [nvarchar](1333) NULL,
	[customdec1] [decimal](12, 7) NULL,
	[customdec2] [decimal](12, 7) NULL,
	[customtext1] [ntext] NULL,
	[customtext2] [ntext] NULL,
	[customtext3] [ntext] NULL,
	[customtext4] [ntext] NULL,
	[timecreated] [datetime2](7) NULL,
	[timemodified] [datetime2](7) NULL,
 CONSTRAINT [PK_T4W_mdl_enrol] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_role]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_role](
	[id] [bigint] NOT NULL,
	[name] [nvarchar](255) NULL,
	[shortname] [nvarchar](100) NULL,
	[description] [ntext] NULL,
	[sortorder] [bigint] NULL,
	[archetype] [nvarchar](30) NULL,
 CONSTRAINT [PK_T4W_mdl_role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_role_assignments]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_role_assignments](
	[id] [bigint] NOT NULL,
	[roleid] [bigint] NULL,
	[contextid] [bigint] NULL,
	[userid] [bigint] NULL,
	[timemodified] [datetime2](7) NULL,
	[modifierid] [bigint] NULL,
	[component] [nvarchar](100) NULL,
	[itemid] [bigint] NULL,
	[sortorder] [bigint] NULL,
 CONSTRAINT [PK_T4W_mdl_role_assignments] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_user]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_user](
	[id] [bigint] NOT NULL,
	[auth] [nvarchar](20) NULL,
	[confirmed] [tinyint] NULL,
	[policyagreed] [tinyint] NULL,
	[deleted] [tinyint] NULL,
	[suspended] [tinyint] NULL,
	[mnethostid] [bigint] NULL,
	[username] [nvarchar](100) NULL,
	[password] [nvarchar](255) NULL,
	[idnumber] [nvarchar](255) NULL,
	[firstname] [nvarchar](100) NULL,
	[lastname] [nvarchar](100) NULL,
	[email] [nvarchar](100) NULL,
	[emailstop] [tinyint] NULL,
	[icq] [nvarchar](15) NULL,
	[skype] [nvarchar](50) NULL,
	[yahoo] [nvarchar](50) NULL,
	[aim] [nvarchar](50) NULL,
	[msn] [nvarchar](50) NULL,
	[phone1] [nvarchar](20) NULL,
	[phone2] [nvarchar](20) NULL,
	[institution] [nvarchar](255) NULL,
	[department] [nvarchar](255) NULL,
	[address] [nvarchar](255) NULL,
	[city] [nvarchar](120) NULL,
	[country] [nvarchar](2) NULL,
	[lang] [nvarchar](30) NULL,
	[theme] [nvarchar](50) NULL,
	[timezone] [nvarchar](100) NULL,
	[firstaccess] [datetime2](7) NULL,
	[lastaccess] [datetime2](7) NULL,
	[lastlogin] [datetime2](7) NULL,
	[currentlogin] [datetime2](7) NULL,
	[lastip] [nvarchar](45) NULL,
	[date_created] [datetime2](7) NULL,
	[secret] [nvarchar](15) NULL,
	[picture] [bigint] NULL,
	[url] [nvarchar](255) NULL,
	[description] [nvarchar](max) NULL,
	[descriptionformat] [tinyint] NULL,
	[mailformat] [tinyint] NULL,
	[maildigest] [tinyint] NULL,
	[maildisplay] [tinyint] NULL,
	[autosubscribe] [tinyint] NULL,
	[trackforums] [tinyint] NULL,
	[timecreated] [datetime2](7) NULL,
	[timemodified] [datetime2](7) NULL,
	[trustbitmask] [bigint] NULL,
	[imagealt] [nvarchar](255) NULL,
	[lastnamephonetic] [nvarchar](255) NULL,
	[firstnamephonetic] [nvarchar](255) NULL,
	[middlename] [nvarchar](255) NULL,
	[alternatename] [nvarchar](255) NULL,
	[calendartype] [nvarchar](30) NULL,
 CONSTRAINT [PK_T4W_mdl_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_user_enrolments]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_user_enrolments](
	[id] [bigint] NOT NULL,
	[status] [bigint] NULL,
	[enrolid] [bigint] NULL,
	[userid] [bigint] NULL,
	[timestart] [datetime2](7) NULL,
	[timeend] [datetime2](7) NULL,
	[modifierid] [bigint] NULL,
	[timecreated] [datetime2](7) NULL,
	[timemodified] [datetime2](7) NULL,
 CONSTRAINT [PK_T4W_mdl_user_enrolments] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_mdl_user_info_data]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_mdl_user_info_data](
	[id] [bigint] NOT NULL,
	[userid] [bigint] NULL,
	[fieldid] [bigint] NULL,
	[data] [nvarchar](max) NULL,
	[dataformat] [tinyint] NULL,
 CONSTRAINT [PK_ T4W_mdl_user_info_data] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_orders]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_orders](
	[id] [bigint] NOT NULL,
	[parent_id] [int] NULL,
	[order_date] [datetime2](7) NULL,
	[order_date_epoch] [datetime2](7) NULL,
	[total] [decimal](13, 3) NULL,
	[payment_method] [nvarchar](20) NULL,
	[transaction_id] [nvarchar](20) NULL,
	[numberOfCourses] [tinyint] NULL,
	[instructions] [nvarchar](250) NULL,
	[paypal_email] [nvarchar](100) NULL,
	[po_number] [nvarchar](40) NULL,
	[refund_transaction_id] [nvarchar](20) NULL,
	[po_date] [datetime2](7) NULL,
	[order_type] [nvarchar](1) NULL,
	[refund_reason] [nvarchar](50) NULL,
	[sale_date] [datetime2](7) NULL,
	[sale_date_epoch] [datetime2](7) NULL,
	[refund_date] [datetime2](7) NULL,
	[refund_amount] [decimal](10, 0) NULL,
	[school_id] [int] NULL,
	[errors] [nvarchar](256) NULL,
	[tracking_code_id] [int] NULL,
	[isT4L] [int] NULL,
 CONSTRAINT [PK_T4W_orders] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_orders_items]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_orders_items](
	[id] [int] NOT NULL,
	[order_id] [int] NULL,
	[course_id] [int] NULL,
	[student_id] [int] NULL,
	[enroll_comment] [nvarchar](250) NULL,
	[created_on] [datetime2](7) NULL,
	[enrolled_on] [datetime2](7) NULL,
	[item_type] [nvarchar](1) NULL,
	[item_price] [decimal](10, 2) NULL,
	[extension_days] [int] NULL,
	[extension_end_date] [datetime2](7) NULL,
	[extension_raid] [int] NULL,
	[extended] [tinyint] NULL,
	[dormancy_id] [int] NULL,
 CONSTRAINT [PK_T4W_orders_items] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_orders_items_refunds]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_orders_items_refunds](
	[id] [int] NOT NULL,
	[our_transaction_id] [int] NULL,
	[oi_itemid] [int] NULL,
	[amount] [decimal](10, 0) NULL,
	[unenrolled] [int] NULL,
	[is_void] [int] NULL,
 CONSTRAINT [PK_T4W_orders_items_refunds] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T4W_parent]    Script Date: 11/20/2020 10:38:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T4W_parent](
	[id] [bigint] NOT NULL,
	[first] [nvarchar](30) NULL,
	[last] [nvarchar](30) NULL,
	[email] [nvarchar](100) NULL,
	[submit_date] [datetime2](7) NULL,
	[quantity_purchased] [int] NULL,
	[howduhear] [nvarchar](255) NULL,
	[country] [nvarchar](3) NULL,
	[newsletter] [tinyint] NULL,
	[street] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[postalcode] [nvarchar](20) NULL,
	[state] [nvarchar](20) NULL,
	[state_id] [int] NULL,
	[country_id] [int] NULL,
	[phone] [nvarchar](20) NULL,
	[fax] [nvarchar](20) NULL,
	[business] [nvarchar](50) NULL,
	[date_created] [datetime2](7) NULL,
	[password] [nvarchar](36) NULL,
	[role] [nvarchar](10) NULL,
	[alternateemail] [nvarchar](100) NULL,
	[usertype] [nvarchar](45) NULL,
 CONSTRAINT [PK_T4W_parent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


